import numpy as np
import control
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, RadioButtons
from matplotlib.patches import Rectangle

# Bigger figures
plt.rcParams["figure.figsize"] = map(
    lambda x: 1.5 * x, plt.rcParamsDefault["figure.figsize"]
    )

CONTROLLERS = ['PD', 'Phase Lead', 'Phase Lag']
CONSTANTS = {
    # Process Constants
    'Kf': {
        'valmin': 0,
        'valmax': 10,
        'valinit': 1,
        'label': '$K_f$',
        },
    'Ja': {
        'valmin': 0,
        'valmax': 10,
        'valinit': 2,
        'label': '$J_a$',
        },
    'Jm': {
        'valmin': 0,
        'valmax': 10,
        'valinit': 2,
        'label': '$J_m$',
        },
    # Controller Constants
    'Kp': {
        'valmin': -5,
        'valmax': 10,
        'valinit': 2,
        'label': '$K_p$',
        },
    'Kv': {
        'valmin': -5,
        'valmax': 10,
        'valinit': 2,
        'label': '$K_v$',
        },
    'T': {
        'valmin': -5,
        'valmax': 10,
        'valinit': 2,
        'label': '$T$',
        },
    'alpha': {
        'valmin': 0,
        'valmax': 1,
        'valinit': 0.5,
        'label': r'$\alpha$',
        },
    'beta': {
        'valmin': 1,
        'valmax': 10,
        'valinit': 2,
        'label': r'$\beta$',
        },
    }


def system(controller, Kf, Ja, Jm, Kp, Kv, T, alpha, beta):
    wa = np.sqrt(Kf / Ja)
    wm = np.sqrt(Kf / Ja * (1 + Ja / Jm))

    # Motor portion
    M = control.tf([1, 0, wa**2], [1, 0, wm**2, 0, 0])

    # Arm portion
    A = control.tf([wa**2], [1, 0, wa**2])

    # Controllers
    if controller == 'PD':
        K = control.tf([Kv, Kp], [1])
    elif controller == 'Phase Lead':
        K = control.tf([alpha * T, alpha], [alpha * T, 1])
    elif controller == 'Phase Lag':
        K = control.tf([T, 1], [beta * T, 1])
    else:
        raise AttributeError(f'Controller {controller} is not valid')

    # Transfer function
    return control.series(control.feedback(control.series(K, M), 1), A)


def update_step(ax, tf, step_range_slider):
    t, y = control.step_response(
        tf, T=np.linspace(0, step_range_slider.val, 1000)
        )
    ax.clear()
    ax.grid()
    ax.set_ylabel('Magnitude')
    ax.set_xlabel('Time [s]')
    ax.hlines(
        1, 0, 200, linestyles='dashed', colors='r', label=r'$\theta_{cmd}$'
        )
    ax.plot(t, y, label=r'$\theta_a$')
    ax.set_xlim(0, step_range_slider.val)
    ax.legend()
    ax.title.set_text('Step Response')
    return ax


def update_pzmap(ax, tf):
    ax.clear()
    ax.tick_params(labelbottom=False, labelleft=False)
    ax.axhline(y=0, color='black', lw=1)
    ax.axvline(x=0, color='black', lw=1)
    ax.axis('equal')
    if len(p := tf.pole()) > 0:
        ax.scatter(
            np.real(p),
            np.imag(p),
            s=50,
            marker='x',
            facecolors='k',
            label='poles'
            )
    if len(z := tf.zero()) > 0:
        ax.scatter(
            np.real(z),
            np.imag(z),
            s=50,
            marker='o',
            edgecolors='k',
            facecolors='none',
            label='zeros'
            )
    r = np.real(np.concatenate([z, p]))
    ax.set_xlim(np.min([1.1 * np.min(r), -1]), np.max([1.1 * np.max(r), 1]))
    ax.legend()
    ax.title.set_text('Pole/Zero map')
    return ax


if __name__ == '__main__':
    # Setup Plots
    fig, (ax_step, ax_poles) = plt.subplots(1, 2)
    plt.subplots_adjust(
        bottom=0.1 + 0.05 * len(CONSTANTS),
        left=0.075,
        right=0.95,
        wspace=0.05
        )

    # Interactive controls
    axcolor = 'lightgoldenrodyellow'
    sliders = dict()
    for i, (label, attributes) in enumerate(CONSTANTS.items()):
        sliders[label] = Slider(
            ax=plt.axes([0.3, 0.05 * (len(CONSTANTS) - i), 0.6, 0.03],
                        facecolor=axcolor),
            **attributes,
            )
    radio_bttn = RadioButtons(
        ax=plt.axes([0.025, 0.025, 0.2, 0.05 * len(CONSTANTS)], frameon=False),
        labels=CONTROLLERS
        )
    step_range_slider = Slider(
        ax=plt.axes([0.15, 0.95, 0.3, 0.03], facecolor=axcolor),
        valmin=10,
        valmax=200,
        valinit=100,
        label='xrange'
        )

    # Define update function
    def update(*args, **kwargs):
        controller = radio_bttn.value_selected
        tf = system(
            controller=controller, **{k: s.val
                                      for k, s in sliders.items()}
            )
        # Update the visibility of interactive controls
        for c in ['alpha', 'beta', 'T', 'Kv', 'Kp']:
            sliders[c].ax.set_visible(False)
        if controller == 'PD':
            sliders['Kv'].ax.set_visible(True)
            sliders['Kp'].ax.set_visible(True)
        elif controller == 'Phase Lead':
            sliders['alpha'].ax.set_visible(True)
            sliders['T'].ax.set_visible(True)
        elif controller == 'Phase Lag':
            sliders['beta'].ax.set_visible(True)
            sliders['T'].ax.set_visible(True)
        else:
            raise AttributeError(f'Controller {controller} is not valid')
        # Step Response Feedback System (Tracking System)
        update_step(ax_step, tf, step_range_slider)
        # Poles and zeros
        update_pzmap(ax_poles, tf)

    # Interactive update
    for s in sliders.values():
        s.on_changed(update)
    step_range_slider.on_changed(update)
    radio_bttn.on_clicked(update)

    # Initial update
    update()

    plt.show()