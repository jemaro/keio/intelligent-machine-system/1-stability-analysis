# 1 Stability Analysis

Andreu Gimenez Bolinches ([andreu@keio.jp](mailto:andreu@keio.jp))

![assignment](./images/assignment.jpg)
\ 

## Approach

I built a simple simulation tool using Python which can be found in my [code
repository](https://gitlab.com/jemaro/keio/intelligent-machine-system/1-stability-analysis).

First I simplified the block diagram in order to obtain a transfer function to
simulate. Then I calculated the step response and pole-zero map for each
controller over a wide range of control and system parameters.

## Assumptions

$$ J_m > 0 $$
$$ J_a > 0 $$
$$ K_f > 0 $$

## Block diagram simplification

The original assignment block diagram is simplified into a transfer function as
it is illustrated here:

![simplification](./images/simplification.drawio.png)
\ 

## PD Controller

The system is stable if $K_p \geq 0$ and $K_v > 0$:

![PD_stable](images/PD_stable.png)
\ 

![PD stable when $K_p = 0$](images/PD_stable_Kp0.png)
\ 

It is marginally stable when $K_v = 0$:

![PD_marginally_stable](images/PD_marginally_stable_Kv0.png)
\ 

Unstable if either $K_v < 0$ or $K_p < O$

![PD_unstable](images/PD_unstable.png)
\ 

![PD_unstable](images/PD_unstable_Kp.png)
\ 

## Phase Lead Controller

The system is stable if $T > 0$ and $0 < \alpha < 1$:

![PhaseLead_stable](images/PhaseLead_stable.png)
\ 

It is marginally stable when $T = 0$ or $\alpha = 1$:

![PhaseLead_marginally_stable](images/PhaseLead_marginally_stable.png)
\ 

![PhaseLead_marginally_stable](images/PhaseLead_marginally_stable_alpha.png)
\ 

Unstable if $T < 0$: 

![PhaseLead_unstable](images/PhaseLead_unstable.png)
\ 

## Phase Lag Controller

The system is unstable if $\beta > 1$:

![PhaseLag_unstable](images/PhaseLag_unstable.png)
\ 

It is marginally stable when $T = 0$ or $\beta = 1$:

![PhaseLag_marginally_stable](images/PhaseLag_marginally_stable_T0.png)
\ 
![PhaseLag_marginally_stable](images/PhaseLag_marginally_stable.png)
\ 

## Summary

As long as the system constant [assumptions](#assumptions) are satisfied, a PD
controller will stabilize the system if the proportional gain is null or
positive and differential gain is positive; a Phase Lead controller will
stabilize the system as long as the compensator gain is positive; a Phase Lag
controller will unstabilize the system unless the compensator gain is null
(which means that there is effectively no controller).

Changes in system constants $K_f$, $J_a$ and $J_m$ will not unstabilize the
analyzed system as long as they are positive and not zero. But they affect
significantly the control performance parameters like the overshot and the
response time.
